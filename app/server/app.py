from fastapi import FastAPI, Request
from fastapi.responses import FileResponse, HTMLResponse
from fastapi.templating import Jinja2Templates

import os

app = FastAPI()
templates = Jinja2Templates(directory="templates")


@app.get("/", response_class=HTMLResponse)
async def read_item(request: Request):
    dirs = os.listdir("./download/")
    return templates.TemplateResponse("item.html", {"request": request, "files": dirs})


@app.get("/test", tags=["test"])
async def test():
    return "hello world"


@app.get("/test/download/{filename}", tags=["test"])
async def test_download(filename: str = None):
    return FileResponse(f"./download/{filename}")
