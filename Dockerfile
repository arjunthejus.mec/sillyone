FROM tiangolo/uvicorn-gunicorn:python3.8

LABEL maintainer="Arjun Thejus <arjunthejus@gmail.com>"

WORKDIR /app

# Install any needed packages specified in requirements.txt
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

# Run the module when the container launches
COPY app/ /app/
CMD python -m server